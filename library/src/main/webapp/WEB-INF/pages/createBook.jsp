<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<body>
<form action="create" method="POST">
    Book Name: <input type="text" name="title" >
    <br />
    price: <input type="number" name="price"/>
    <br />
    Publishing date: <input type="date" name="publishDate" name="birthday"/>
    <br />
    Author: <select id="Author" name="Author">
                <c:forEach items="${authors}" var="author">
                    <option value="${author.id}">${author.name}</option>
                </c:forEach>
            </select>
    <input type="submit" value="Submit" />
</form>
</body>
</html>