<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>

<body>
    <h1>BOOK</h1>
    <ul>
        <li><p><b>Title:</b>
            ${book.title}
        </p></li>
        <li><p><b>price:</b>
            ${book.price}
        </p></li>
        <li><p><b>publication date:</b>
            ${book.publicationDate}
        </p></li>
        <li><p><b>author:</b>
            ${book.author.name}
        </p></li>
    </ul>

    <sec:authorize access="hasRole('ROLE_ADMIN')">
    <a href="${book.id}/delete">delete</a>
    </sec:authorize>
</body>
</html>