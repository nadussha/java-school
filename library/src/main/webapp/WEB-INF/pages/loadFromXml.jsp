<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: mshvarku
  Date: 22.04.2015
  Time: 9:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title></title>
</head>
<body>
<c:choose>
  <c:when test="${loadingSuccessful == true}">
    <h1>Loading successful</h1>
  </c:when>

  <c:otherwise>
    <h1>Loading failed</h1>
    <br/>
    <p>${loadingMsg}</p>
  </c:otherwise>
</c:choose>
</body>
</html>
