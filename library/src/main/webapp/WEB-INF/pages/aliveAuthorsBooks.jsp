<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<style type="text/css">
  th {
    color: white;
  }
</style>

<p>
  Alive authors books
</p>

<%--<form action="/editBook/find" method="GET">--%>
<%--Book Name: <input type="text" name="name" >--%>
<%--<br />--%>
<%--</form>--%>

<table style="border: 1px solid black;" cellpadding="6" cellspacing="0">
  <tr valign="baseline" bgcolor="#404060">
    <th align="center"> ID</th>
    <th align="left"> Title</th>
    <th align="left"> Author</th>
    <th align="center"> VIEW</th>
  </tr>

  <c:forEach var="book" items="${aliveAuthorsBooks}" varStatus="lineInfo">

    <c:choose>
      <c:when test="${lineInfo.count % 2 == 0}"> <tr bgcolor="#f7f7e7"> </c:when>
      <c:otherwise> <tr bgcolor="white"> </c:otherwise>
    </c:choose>

    <td align="center"> ${book.id} </td>
    <td align="left"> ${book.title} </td>
    <td align="left"><a href="/authors/${book.author.id}"> ${book.author.name} </td>
    <td align="left"><a href="/books/${book.id}">view</a></td>
    </tr>

  </c:forEach>
</table>