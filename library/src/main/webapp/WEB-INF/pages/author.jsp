<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>

<body>
<h1>Author</h1>
<ul>
  <li><p><b>Name:</b>
    ${author.name}
  </p></li>
  <li><p><b>Birthday:</b>
    ${author.birthDay}
  </p></li>
  <li><p><b>Death day:</b>
    ${author.deathDay}
  </p></li>
</ul>

<sec:authorize access="hasRole('ROLE_ADMIN')">
  <a href="${author.id}/delete">delete</a>
</sec:authorize>
</body>
</html>