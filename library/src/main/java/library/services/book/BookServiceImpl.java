package library.services.book;

import library.dao.book.BookDAO;
import library.objects.Author;
import library.objects.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by mshvarku on 21.04.2015.
 */
@Service
public class BookServiceImpl implements BookService {
    @Autowired
    BookDAO bookDAO;
    private final Logger logger = LoggerFactory.getLogger(BookServiceImpl.class);

    @Override
    @Transactional
    public int addBook(Book book) {
        int i = bookDAO.addBook(book);
        logger.info("Book {} ({}) added", i, book.getTitle());
        return i;
    }

    @Override
    @Cacheable(value = "bookCache", key = "#id")
    public Book getBook(int id) {
        return bookDAO.getBook(id);
    }

    @Override
    @Transactional
    @CacheEvict(value = "bookCache", key = "#id")
    public void deleteBook(int id) {
        logger.info("Book {} deleted", id);
        bookDAO.deleteBook(id);
    }

    @Override
    @Transactional
    @CacheEvict(value = "bookCache", allEntries = true)
    public void deleteAllBooks() {
        logger.info("All books deleted");
        bookDAO.deleteAllBooks();
    }

    @Override
    public List<Book> getAllBooks() {
        return bookDAO.getAllBooks();
    }

    @Override
    public List<Book> getBooksBoundedWithPrice(int bound) {
        return bookDAO.getBooksBoundedWithPrice(bound);
    }

    @Override
    public List<Book> getAllBooksOfAuthor(Author author) {
        return bookDAO.getBooksOfAuthor(author.getId());
    }

    @Override
    public List<Book> getAllBooksOfAuthor(int id) {
        return bookDAO.getBooksOfAuthor(id);
    }

    @Override
    public List<Book> getBooksOfAliveAuthors() {
        return bookDAO.getBooksOfAliveAuthors();
    }

    @Override
    public void setBookDAO(BookDAO bookDAO) {
        this.bookDAO = bookDAO;
    }
}
