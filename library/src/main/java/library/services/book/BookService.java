package library.services.book;

import library.dao.book.BookDAO;
import library.objects.Author;
import library.objects.Book;
import java.util.List;

/**
 * Created by mshvarku on 21.04.2015.
 */
public interface BookService {
    int addBook(Book book);

    Book getBook(int id);

    void deleteBook(int book);

    void deleteAllBooks();

    List<Book> getAllBooks();

    List<Book> getBooksBoundedWithPrice(int bound);

    List<Book> getAllBooksOfAuthor(Author author);

    List<Book> getAllBooksOfAuthor(int id);

    List<Book> getBooksOfAliveAuthors();

    void setBookDAO(BookDAO bookDAO);
}
