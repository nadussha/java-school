package library.services.xml;

import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.JAXBException;

/**
 * Created by mshvarku on 21.04.2015.
 */
public interface XmlBookService {
    void saveBooksToXML() throws Exception;
    void getBooksFromXML() throws JAXBException;
}
