package library.services.xml.adapters.BookAdapter;

import library.objects.Author;

import java.util.Date;

/**
 * Created by mshvarku on 21.04.2015.
 */
public class AdaptedBook {
    private String title;
    private int id;
    private Author author;
    private int price;
    private Date publicationDate;

    public int getId() {
        return id;
    }

    public AdaptedBook setId(int id) {
        this.id = id;
        return this;
    }

    public Author getAuthor() {
        return author;
    }

    public AdaptedBook setAuthor(Author author) {
        this.author = author;
        return this;
    }

    public int getPrice() {
        return price;
    }

    public AdaptedBook setPrice(int price) {
        this.price = price;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public AdaptedBook setTitle(String title) {
        this.title = title;
        return this;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public AdaptedBook setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
        return this;
    }
}
