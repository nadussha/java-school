package library.services.xml.adapters.authorAdapter;

import java.util.Date;

/**
 * Created by mshvarku on 21.04.2015.
 */
public class AdaptedAuthor {
    private int id;
    private String name;
    private Date birthDay;
    private Date deathDay;

    public Date getDeathDay() {
        return deathDay;
    }

    public AdaptedAuthor setDeathDay(Date deathDay) {
        this.deathDay = deathDay;
        return this;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public AdaptedAuthor setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
        return this;
    }

    public String getName() {
        return name;
    }

    public AdaptedAuthor setName(String name) {
        this.name = name;
        return this;
    }

    public int getId() {
        return id;
    }

    public AdaptedAuthor setId(int id) {
        this.id = id;
        return this;
    }

}
