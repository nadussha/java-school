package library.services.xml.adapters.authorAdapter;

import library.objects.Author;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Created by mshvarku on 21.04.2015.
 */
public class AuthorAdapter extends XmlAdapter<AdaptedAuthor, Author> {
    @Override
    public Author unmarshal(AdaptedAuthor v) throws Exception {
        return Author.newBuilder().id(v.getId()).birthDay(v.getBirthDay())
                .deathDay(v.getDeathDay()).name(v.getName()).build();
    }

    @Override
    public AdaptedAuthor marshal(Author v) throws Exception {
        AdaptedAuthor adaptedAuthor = new AdaptedAuthor();
        adaptedAuthor.setId(v.getId());
        adaptedAuthor.setBirthDay(v.getBirthDay());
        adaptedAuthor.setDeathDay(v.getDeathDay());
        adaptedAuthor.setName(v.getName());
        return adaptedAuthor;
    }
}
