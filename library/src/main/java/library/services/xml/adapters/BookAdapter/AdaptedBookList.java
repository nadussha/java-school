package library.services.xml.adapters.BookAdapter;

import library.objects.Book;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

/**
 * Created by mshvarku on 21.04.2015.
 */
@XmlRootElement(name = "books")
@XmlAccessorType(XmlAccessType.FIELD)
public class AdaptedBookList {
    @XmlElement(name = "book")
    List<Book> bookList;

    public List<Book> getBookList() {
        return bookList;
    }

    public AdaptedBookList setBookList(List<Book> bookList) {
        this.bookList = bookList;
        return this;
    }
}
