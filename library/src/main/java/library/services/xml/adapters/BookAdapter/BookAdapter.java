package library.services.xml.adapters.BookAdapter;

import library.objects.Book;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Created by mshvarku on 21.04.2015.
 */
public class BookAdapter extends XmlAdapter<AdaptedBook, Book> {
    @Override
    public Book unmarshal(AdaptedBook v) throws Exception {
        return Book.newBuilder().author(v.getAuthor()).publicationDate(v.getPublicationDate())
                .title(v.getTitle()).price(v.getPrice()).id(v.getId()).build();
    }

    @Override
    public AdaptedBook marshal(Book v) throws Exception {
        AdaptedBook adaptedBook = new AdaptedBook();
        adaptedBook.setId(v.getId());
        adaptedBook.setAuthor(v.getAuthor());
        adaptedBook.setPrice(v.getPrice());
        adaptedBook.setPublicationDate(v.getPublicationDate());
        adaptedBook.setTitle(v.getTitle());
        return adaptedBook;
    }
}
