package library.services.xml;

import library.objects.Book;
import library.services.author.AuthorService;
import library.services.xml.adapters.BookAdapter.AdaptedBookList;
import library.services.book.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.*;

/**
 * Created by mshvarku on 21.04.2015.
 */
@Service
public class XmlBookServiceImpl implements XmlBookService {

    private final String XML_FILE_NAME = "C:\\Users\\mshvarku\\Desktop\\allBooks.xml";

    @Autowired
    private BookService bookService;
    @Autowired
    private AuthorService authorService;

    private final Logger logger = LoggerFactory.getLogger(XmlBookServiceImpl.class);

    @Override
    public void saveBooksToXML() throws Exception {
        List<Book> allBooks = bookService.getAllBooks();

        JAXBContext jaxbContext = JAXBContext.newInstance(AdaptedBookList.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(new AdaptedBookList().setBookList(allBooks), new File(XML_FILE_NAME));
        logger.info("Books saved to file {}", XML_FILE_NAME);
    }

    @Override
    @Transactional
    public void getBooksFromXML() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(AdaptedBookList.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        AdaptedBookList books = (AdaptedBookList) unmarshaller.unmarshal(new File(XML_FILE_NAME));
        List<Book> bookList = books.getBookList();
        authorService.deleteAllAuthors();
        bookService.deleteAllBooks();
        saveBooksToDB(bookList);
        logger.info("Books loaded from file {}", XML_FILE_NAME);
    }

    private void saveBooksToDB(List<Book> bookList) {
        Set<Integer> authorIdx = new HashSet<>();
        for (Book book : bookList) {
            if (!authorIdx.contains(book.getAuthor().getId())) {
                authorService.addAuthor(book.getAuthor());
            }
            bookService.addBook(book);
        }
    }
}
