package library.services.author;

import library.dao.author.AuthorDAO;
import library.objects.Author;
import java.util.List;

/**
 * Created by mshvarku on 21.04.2015.
 */
public interface AuthorService {
    void setAuthorDAO(AuthorDAO authorDAO);
    Author getAuthor(int id);
    List<Author> getAllAuthors();
    int addAuthor(Author author);
    void deleteAuthor(int id);
    void deleteAllAuthors();
}
