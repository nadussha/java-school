package library.services.author;

import library.dao.author.AuthorDAO;
import library.objects.Author;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by mshvarku on 21.04.2015.
 */
@Service
public class AuthorServiceImpl implements AuthorService {

    AuthorDAO authorDAO;
    private final Logger logger = LoggerFactory.getLogger(AuthorServiceImpl.class);

    @Autowired
    @Override
    public void setAuthorDAO(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    @Override
    @Cacheable(value = "authorCache", key = "#id")
    public Author getAuthor(int id) {
        return authorDAO.getAuthor(id);
    }

    @Override
    public List<Author> getAllAuthors() {
        return authorDAO.getAllAuthors();
    }

    @Override
    @Transactional
    public int addAuthor(Author author) {
        int id = authorDAO.addAuthor(author);
        author.setId(id);
        logger.info("Author {} ({}) added", id, author.getName());
        return id;
    }

    @Override
    @Transactional
    @CacheEvict(value = "authorCache", key = "#id")
    public void deleteAuthor(int id) {
        logger.info("Author {} deleted", id);
        authorDAO.deleteAuthor(id);
    }

    @Override
    @Transactional
    @CacheEvict(value = "authorCache", allEntries = true)
    public void deleteAllAuthors() {
        logger.info("All authors deleted");
        authorDAO.deleteAllAuthors();
    }
}
