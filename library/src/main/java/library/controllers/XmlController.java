package library.controllers;

import library.services.xml.XmlBookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by mshvarku on 22.04.2015.
 */
@Controller
@RequestMapping("/xml")
public class XmlController extends MainController {

    @Autowired
    XmlBookService xmlBookService;

    private final Logger logger = LoggerFactory.getLogger(XmlController.class);

    @RequestMapping(value = "/saveToXml")
    public String saveBooksToXml(ModelMap model) {
        try {
            xmlBookService.saveBooksToXML();
            model.addAttribute("savingSuccessful", true);
        } catch (Exception e) {
            logger.error("Failed saving books to xml: {}", e);
            model.addAttribute("savingSuccessful", false);
            model.addAttribute("savingMsg", e.toString());
        }
        return "saveToXml";
    }

    @RequestMapping(value = "/loadFromXml")
    public String loadBooksFromXml(ModelMap model) {
        try {
            xmlBookService.getBooksFromXML();
            model.addAttribute("loadingSuccessful", true);
        } catch (Exception e) {
            logger.error("Failed loading books from xml: {}", e);
            model.addAttribute("loadingSuccessful", false);
            model.addAttribute("loadingMsg", e.toString());
        }
        return "loadFromXml";
    }
}
