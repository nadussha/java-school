package library.controllers;

import library.objects.Author;
import library.objects.Book;
import library.services.author.AuthorService;
import library.services.book.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by mshvarku on 21.04.2015.
 */
@Controller
@RequestMapping("/editBook")
public class BookController extends MainController {

    @Autowired
    BookService bookService;

    @Autowired
    AuthorService authorService;
    private final Logger logger = LoggerFactory.getLogger(BookController.class);

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createBook(ModelMap model) {
        model.addAttribute("authors", authorService.getAllAuthors());
        return "createBook";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createBook(HttpServletRequest request, ModelMap model) {
        DateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
        try {
            String authorID = request.getParameter("Author");
            int id = Integer.parseInt(authorID);
            Author author = authorService.getAuthor(id);
            String publishedString = request.getParameter("publishDate");
            Date publishedDate = null;
            if (publishedString.length() != 0) {
                publishedDate = format.parse(publishedString);
            }
            Book book = Book.newBuilder().price(Integer.parseInt(request.getParameter("price")))
                    .title(request.getParameter("title"))
                    .publicationDate(publishedDate)
                    .author(author).build();
            bookService.addBook(book);
        } catch (ParseException e) {
            logger.error("Error parsing Date: {}", e);
            throw new RuntimeException(e);
        }

        return showMainPage(model);
    }
}
