package library.controllers;

import library.services.author.AuthorService;
import library.services.book.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by mshvarku on 21/04/2015.
 */
@Controller
@RequestMapping("/authors")
public class AuthorBookController extends MainController{
    @Autowired
    AuthorService authorService;

    @RequestMapping("/{id}")
    public String findBook(@PathVariable long id, ModelMap model){
        model.addAttribute("author", authorService.getAuthor((int) id));
        return "author";
    }

    @RequestMapping("/{id}/delete")
    public String deleteBook(@PathVariable long id, ModelMap model) {
        authorService.deleteAuthor((int) id);
        return showMainPage(model);
    }
}






