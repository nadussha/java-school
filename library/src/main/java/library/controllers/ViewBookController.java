package library.controllers;

import library.services.book.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by mshvarku on 21/04/2015.
 */
@Controller
@RequestMapping("/books")
public class ViewBookController extends MainController{
    @Autowired
    private BookService bookService;

    @RequestMapping("/{id}")
    public String findBook(@PathVariable long id, ModelMap model){
        System.out.println("id = " + id);
        model.addAttribute("book", bookService.getBook((int) id));
        return "book";
    }

    @RequestMapping("/{id}/delete")
    public String deleteBook(@PathVariable long id, ModelMap model) {
        bookService.deleteBook((int) id);

        return showMainPage(model);
    }

    @RequestMapping("/aliveAuthorsBooks")
    public String showAliveAuthorsBooks(ModelMap model){
        model.addAttribute("aliveAuthorsBooks", bookService.getBooksOfAliveAuthors());
        return "aliveAuthorsBooks";
    }
}






