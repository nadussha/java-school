package library.controllers;

import library.services.author.AuthorService;
import library.services.book.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by mshvarku on 21.04.2015.
 */
@Controller
@RequestMapping("/")
public class MainController {

    @Autowired
    private BookService bookService;

    @RequestMapping(method = RequestMethod.GET)
    public String showMainPage(ModelMap model) {
        model.addAttribute("books", bookService.getAllBooks());
        return "main";
    }
}
