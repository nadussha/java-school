package library.controllers;

import library.objects.Author;
import library.services.author.AuthorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by mshvarku on 21.04.2015.
 */
@Controller
@RequestMapping("/editAuthor")
public class AuthorController extends MainController {

    @Autowired
    AuthorService authorService;
    private final Logger logger = LoggerFactory.getLogger(AuthorController.class);

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createAuthor() {
        return "createAuthor";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createAuthor(HttpServletRequest request, ModelMap model) {
        DateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
        try {
            String deathday = request.getParameter("deathday");
            String birthday = request.getParameter("birthday");
            Date deathDate = null;
            Date birthDate = null;
            if (deathday.length() != 0) {
                deathDate = format.parse(deathday);
            }
            if (birthday.length() != 0) {
                birthDate = format.parse(birthday);
            }
            Author author = Author.newBuilder().name(request.getParameter("name"))
                .birthDay(birthDate
                )
                .deathDay(deathDate).build();
            authorService.addAuthor(author);
        } catch (ParseException e) {
            logger.error("Error parsing Date: {}", e);
            throw new RuntimeException(e);
        }

        return showMainPage(model);
    }
}
