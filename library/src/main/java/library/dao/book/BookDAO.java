package library.dao.book;

import library.objects.Author;
import library.objects.Book;

import java.util.Date;
import java.util.List;

/**
 * Created by mshvarku on 21.04.2015.
 */
public interface BookDAO {
    int addBook(Book book);

    void addBatchOfBooks(List<Book> bookList);

    void deleteBook(int id);

    List<Book> getAllBooks();

    List<Book> getBooksBoundedWithPrice(int bound);

    List<Book> getBooksOfAuthor(int id);

    List<Book> getBooksOfAliveAuthors();

    Book getBook(int id);

    void deleteAllBooks();
}
