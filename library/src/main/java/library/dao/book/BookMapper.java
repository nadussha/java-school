package library.dao.book;

import library.dao.author.AuthorMapper;
import library.objects.Author;
import library.objects.Book;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mshvarku on 20.04.2015.
 */
@Component
public class BookMapper implements RowMapper<Book> {

    private Map<Integer, Author> authorMap = new HashMap<>();

    private Author buildAuthor(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("AUTHOR_ID");
        Author author = authorMap.get(id);
        if (author != null) {
            return author;
        }
        Date birthday = resultSet.getDate("BIRTH_DAY");
        Date deathday = resultSet.getDate("DEATH_DAY");
        String name = resultSet.getString("NAME");
        author = Author.newBuilder().name(name).deathDay(deathday).birthDay(birthday).id(id).build();
        authorMap.put(id, author);
        return author;
    }

    @Override
    public Book mapRow(ResultSet resultSet, int i) throws SQLException {
        int id = resultSet.getInt("ID");
        Date publicationDate = resultSet.getDate("PUBLICATION_DATE");
        String title = resultSet.getString("TITLE");
        int price = resultSet.getInt("PRICE");
        Author author = buildAuthor(resultSet);
        return Book.newBuilder().id(id).author(author).publicationDate(publicationDate)
                .title(title).price(price).build();
    }
}
