package library.dao.book;

import library.objects.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mshvarku on 21.04.2015.
 */
@Repository
public class BookDAOImpl implements BookDAO {

    private final String joinSql = " BOOKS INNER JOIN AUTHORS ON BOOKS.AUTHOR_ID = AUTHORS.ID ";
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    SimpleJdbcInsert simpleJdbcInsert;

    @PostConstruct
    private void initTable() {
        simpleJdbcInsert.withTableName("BOOKS").usingGeneratedKeyColumns("ID");
    }

    @Override
    public int addBook(Book book) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("TITLE", book.getTitle());
        parameters.put("PRICE", book.getPrice());
        parameters.put("PUBLICATION_DATE", book.getPublicationDate());
        parameters.put("AUTHOR_ID", book.getAuthor().getId());
        return simpleJdbcInsert.executeAndReturnKey(parameters).intValue();
    }

    @Override
    public void addBatchOfBooks(final List<Book> bookList) {
        String sql = "DELETE FROM BOOKS";
        jdbcTemplate.execute(sql);
        sql = "INSERT INTO BOOKS (TITLE, AUTHOR_ID, PUBLICATION_DATE, PRICE) VALUES (?, ?, ?, ?)";
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Book book = bookList.get(i);
                ps.setString(1, book.getTitle());
                ps.setInt(2, book.getAuthor().getId());
                ps.setDate(3, new java.sql.Date(book.getPublicationDate().getTime()));
                ps.setInt(4, book.getId());
            }

            @Override
            public int getBatchSize() {
                return bookList.size();
            }
        });
    }

    @Override
    public void deleteBook(int id) {
        String sql = "DELETE FROM BOOKS WHERE ID = ?";
        jdbcTemplate.update(sql, id);
    }


    @Override
    public List<Book> getAllBooks() {
        String sql = "SELECT * FROM" + joinSql;
        return jdbcTemplate.query(sql, new BookMapper());
    }

    @Override
    public List<Book> getBooksBoundedWithPrice(int bound) {
        String sql = "SELECT * FROM" + joinSql + "WHERE PRICE > ?";
        return jdbcTemplate.query(sql, new BookMapper(), bound);
    }

    @Override
    public List<Book> getBooksOfAuthor(int id) {
        String sql = "SELECT * FROM  " + joinSql + " WHERE AUTHOR_ID = ?";
        return jdbcTemplate.query(sql, new BookMapper(), id);
    }

    @Override
    public List<Book> getBooksOfAliveAuthors() {
        String sql = "SELECT * FROM" +joinSql + "WHERE AUTHORS.DEATH_DAY IS NULL";
        return jdbcTemplate.query(sql, new BookMapper());
    }

    @Override
    public Book getBook(int id) {
        String sql = "SELECT * FROM" + joinSql + " WHERE BOOKS.ID = ?";
        List<Book> bookList = jdbcTemplate.query(sql, new BookMapper(), id);
        if (bookList.size() == 1) {
            return bookList.get(0);
        }
        return null;
    }

    @Override
    public void deleteAllBooks() {
        String sql = "DELETE FROM BOOKS";
        jdbcTemplate.execute(sql);
    }
}
