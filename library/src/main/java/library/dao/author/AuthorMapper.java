package library.dao.author;

import library.objects.Author;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mshvarku on 21.04.2015.
 */
public class AuthorMapper implements RowMapper<Author> {

    @Override
    public Author mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        int id = resultSet.getInt("ID");
        Date birthday = resultSet.getDate("BIRTH_DAY");
        Date deathday = resultSet.getDate("DEATH_DAY");
        String name = resultSet.getString("NAME");
        return Author.newBuilder().name(name).deathDay(deathday).birthDay(birthday).id(id).build();
    }
}
