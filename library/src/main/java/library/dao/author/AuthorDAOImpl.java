package library.dao.author;

import library.objects.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Created by mshvarku on 21.04.2015.
 */
@Repository
public class AuthorDAOImpl implements AuthorDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    SimpleJdbcInsert simpleJdbcInsert;

    @PostConstruct
    private void initTable() {
        simpleJdbcInsert.withTableName("AUTHORS").usingGeneratedKeyColumns("ID");
    }

    @Override
    public Author getAuthor(int id) {
        String sql = "SELECT * FROM AUTHORS WHERE ID = ?";
        List<Author> query = jdbcTemplate.query(sql, new AuthorMapper(), id);
        if (query.size() == 1) {
            return query.get(0);
        }
        return null;
    }

    @Override
    public List<Author> getAllAuthors() {
        String sql = "SELECT * FROM AUTHORS";
        return jdbcTemplate.query(sql, new AuthorMapper());
    }

    @Override
    public int addAuthor(Author author) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", author.getName());
        parameters.put("BIRTH_DAY", author.getBirthDay());
        parameters.put("DEATH_DAY", author.getDeathDay());
        return simpleJdbcInsert.executeAndReturnKey(parameters).intValue();
    }

    @Override
    public void deleteAuthor(int id) {
        String sql = "DELETE FROM AUTHORS WHERE AUTHORS.ID = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public void deleteAllAuthors() {
        String sql = "DELETE FROM AUTHORS";
        jdbcTemplate.execute(sql);
    }
}
