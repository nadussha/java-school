package library.dao.author;

import library.objects.Author;

import java.util.Date;
import java.util.List;

/**
 * Created by mshvarku on 21.04.2015.
 */
public interface AuthorDAO {

    Author getAuthor(int id);

    List<Author> getAllAuthors();

    int addAuthor(Author author);

    void deleteAuthor(int id);

    void deleteAllAuthors();
}
