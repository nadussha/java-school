package library.objects;

import library.services.xml.adapters.authorAdapter.AuthorAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

/**
 * Created by mshvarku on 20.04.2015.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlJavaTypeAdapter(AuthorAdapter.class)
public class Author {

    public void setId(int id) {
        this.id = id;
    }

    private int id;
    private String name;
    private Date birthDay;
    private Date deathDay;

    private Author(int id, String name, Date birthDay, Date deathDay) {
        this.id = id;
        this.name = name;
        this.birthDay = birthDay;
        this.deathDay = deathDay;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        private int id;
        private String name;
        private Date birthDay;
        private Date deathDay;
        private volatile boolean alreadyUsed = false;

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder birthDay(Date birthDay) {
            this.birthDay = birthDay;
            return this;
        }

        public Builder deathDay(Date deathDay) {
            this.deathDay = deathDay;
            return this;
        }

        public Author build() {
            if (alreadyUsed) {
                throw new RuntimeException("Builder can't be used twice");
            }
            alreadyUsed = true;
            validate();
            return new Author(id, name, birthDay, deathDay);
        }

        private void validate() {
            //todo add validation
        }
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public Date getDeathDay() {
        return deathDay;
    }
}
