package library.objects;

import library.services.xml.adapters.BookAdapter.BookAdapter;
import library.services.xml.adapters.authorAdapter.AuthorAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

/**
 * Created by mshvarku on 20.04.2015.
 */
@XmlJavaTypeAdapter(BookAdapter.class)
@XmlAccessorType(XmlAccessType.FIELD)
public class Book {

    private Author author;
    private int id;
    private int price;
    private String title;
    private Date publicationDate;

    private Book(int id, Author author, int price, String title, Date publicationDate) {
        this.id = id;
        this.author = author;
        this.price = price;
        this.title = title;
        this.publicationDate = publicationDate;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        private int id;
        private Author author;
        private int price;
        private String title;
        private Date publicationDate;
        private volatile boolean alreadyUsed = false;

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder author(Author author) {
            this.author = author;
            return this;
        }

        public Builder price(int price) {
            this.price = price;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder publicationDate(Date publicationDate) {
            this.publicationDate = publicationDate;
            return this;
        }

        public Book build() {
            if (alreadyUsed) {
                throw new RuntimeException("Builder can't be used twice");
            }
            alreadyUsed = true;
            validate();
            return new Book(id, author, price, title,publicationDate);
        }

        private void validate() {
            //todo add validation
        }
    }

    public int getId() {
        return id;
    }

    public Author getAuthor() {
        return author;
    }

    public int getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }
}
