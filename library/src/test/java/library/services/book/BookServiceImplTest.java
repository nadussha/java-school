package library.services.book;

import library.dao.book.BookDAO;
import library.objects.Book;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.*;

/**
 * Created by mshvarku on 22.04.2015.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
public class BookServiceImplTest {

    @Autowired
    private BookService bookService;
    private BookDAO mockDAO;

    @Before
    public void init() {
        mockDAO = mock(BookDAO.class);
        when(mockDAO.getBook(anyInt())).thenReturn(Book.newBuilder().build());
        doAnswer(new Answer() {
            @Override
            public Void answer(InvocationOnMock invocationOnMock) throws Throwable {
                System.out.println("delete done");
                return null;
            }
        }).when(mockDAO).deleteBook(anyInt());
        bookService.setBookDAO(mockDAO);
    }

    @Test
    public void testCache() throws Exception {
        bookService.getBook(5);
        bookService.getBook(5);
        bookService.getBook(5);
        bookService.getBook(3);
        bookService.getBook(3);
        bookService.getBook(3);
        bookService.deleteBook(5);
        bookService.getBook(5);
        bookService.getBook(5);

        verify(mockDAO, times(3)).getBook(anyInt());
    }
}