package library.services.author;

import library.dao.author.AuthorDAO;
import library.objects.Author;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.*;

/**
 * Created by mshvarku on 22.04.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
public class AuthorServiceImplTest {

    @Autowired
    private AuthorService authorService;
    private AuthorDAO mockDAO;

    @Before
    public void init() {
        mockDAO = mock(AuthorDAO.class);
        when(mockDAO.getAuthor(anyInt())).thenReturn(Author.newBuilder().build());
        doAnswer(new Answer() {
            @Override
            public Void answer(InvocationOnMock invocationOnMock) throws Throwable {
                System.out.println("delete done");
                return null;
            }
        }).when(mockDAO).deleteAuthor(anyInt());
        authorService.setAuthorDAO(mockDAO);
    }

    @Test
    public void testCache() throws Exception {
        authorService.getAuthor(5);
        authorService.getAuthor(5);

        authorService.getAuthor(4);
        authorService.getAuthor(4);

        authorService.deleteAuthor(5);

        authorService.getAuthor(5);

        verify(mockDAO, times(3)).getAuthor(anyInt());
    }
}