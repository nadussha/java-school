package library.dao.author;

import library.dao.book.BookMapper;
import library.objects.Author;
import library.objects.Book;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;

import static org.junit.Assert.*;

/**
 * Created by mshvarku on 22.04.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@ActiveProfiles("test")
public class AuthorDAOImplTest {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    AuthorDAO authorDAO;

    @Test
    @Transactional
    public void testAddAndGetAuthor() throws Exception {
        DateTime birthDay = new DateTime(1799, 6, 6, 12, 0, 0, 0);
        DateTime deathDay = new DateTime(1837, 2, 10, 12, 0, 0, 0);
        Author pushkin = Author.newBuilder().name("Pushkin").birthDay(birthDay.toDate()).deathDay(deathDay.toDate()).build();
        int pushkinIndex = authorDAO.addAuthor(pushkin);
        Author author = authorDAO.getAuthor(pushkinIndex);
        assertEquals(pushkinIndex, author.getId());
        assertEquals(pushkin.getName(), author.getName());
    }

    @Test
    @Transactional
    public void testDeleteAuthor() {
        DateTime birthDay = new DateTime(1799, 6, 6, 12, 0, 0, 0);
        DateTime deathDay = new DateTime(1837, 2, 10, 12, 0, 0, 0);
        Author pushkin = Author.newBuilder().name("Pushkin").birthDay(birthDay.toDate()).deathDay(deathDay.toDate()).build();
        int pushkinIndex = authorDAO.addAuthor(pushkin);
        authorDAO.deleteAuthor(pushkinIndex);
        Author author = authorDAO.getAuthor(pushkinIndex);
        assertNull(author);
    }
}