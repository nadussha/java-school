package library.dao.book;

import library.objects.Author;
import library.objects.Book;
import library.services.author.AuthorService;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by mshvarku on 22.04.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@ActiveProfiles("test")
public class BookDAOImplTest {
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    BookDAO bookDAO;

    private Author pushkin;

    @Before
    public void setUp() {
        DateTime birthDay = new DateTime(1799, 6, 6, 12, 0, 0, 0);
        DateTime deathDay = new DateTime(1837, 2, 10, 12, 0, 0, 0);
        pushkin = Author.newBuilder().name("Lermont").birthDay(birthDay.toDate()).deathDay(deathDay.toDate()).id(3001).build();
    }

    @Test
    @Transactional
    public void testAddAndGetBook() throws Exception {
        String title = "Trololo book";
        DateTime publicationDate = new DateTime(1810, 2, 10, 12, 0, 0, 0);
        int price = 700;
        Book pushkinBook = Book.newBuilder().author(pushkin).title(title).author(pushkin).publicationDate(publicationDate.toDate()).price(price).build();
        int idx = bookDAO.addBook(pushkinBook);
        Book book = bookDAO.getBook(idx);
        assertEquals(pushkinBook.getAuthor().getId(), book.getAuthor().getId());
        assertEquals(pushkinBook.getPrice(), book.getPrice());
        assertEquals(pushkinBook.getTitle(), book.getTitle());
        assertEquals(idx, book.getId());
    }

    @Test
    @Transactional
    public void testDeleteBook() {
        String title = "Trololo book";
        DateTime publicationDate = new DateTime(1810, 2, 10, 12, 0, 0, 0);
        int price = 700;
        Book pushkinBook = Book.newBuilder().author(pushkin).title(title).author(pushkin).publicationDate(publicationDate.toDate()).price(price).build();
        int idx = bookDAO.addBook(pushkinBook);
        bookDAO.deleteBook(idx);
        Book book = bookDAO.getBook(idx);
        assertNull(book);
    }
}