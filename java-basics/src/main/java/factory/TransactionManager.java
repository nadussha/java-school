package factory;

/**
 * Created by mshvarku on 15/04/2015.
 */
public interface TransactionManager {
    void openTransaction();
    void commit();
    void rollback();
    boolean isTransactionOpened();
}
