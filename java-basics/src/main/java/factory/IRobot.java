package factory;

import javax.annotation.PostConstruct;

/**
 * Created by mshvarku on 13/04/2015.
 */
public class IRobot {

    @PostConstruct
    public void init() {
        System.out.println(cleaner.getClass());
    }

    @Inject
    private Cleaner cleaner;

    @Inject
    private Speaker speaker;



    public void cleanRoom(){
        speaker.speak("I started my work");
        cleaner.clean();
        speaker.speak("I finished my work");
    }
}
