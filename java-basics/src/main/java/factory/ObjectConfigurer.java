package factory;

/**
 * Created by mshvarku on 14/04/2015.
 */
public interface ObjectConfigurer {
    void configure(Object o) throws Exception;
}
