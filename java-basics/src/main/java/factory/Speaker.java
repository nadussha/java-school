package factory;

/**
 * Created by mshvarku on 13/04/2015.
 */
public interface Speaker {
    void speak(String message);
}
