package factory;

/**
 * Created by mshvarku on 13/04/2015.
 */
public interface Cleaner {
    void clean();
}
