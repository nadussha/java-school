package factory;

/**
 * Created by mshvarku on 15/04/2015.
 */
public class TransactionManagerImpl implements TransactionManager {
    private static TransactionManager ourInstance = new TransactionManagerImpl();

    public static TransactionManager getInstance() {
        return ourInstance;
    }

    private TransactionManagerImpl() {
    }


    private int depth = 0;

    @Override
    public void openTransaction() {
        System.out.println("*************TRANSACTION OPENED***************");
        depth++;
    }

    @Override
    public void commit() {
        System.out.println("*************TRANSACTION COMMITTED***************");
        depth--;
    }

    @Override
    public void rollback() {
        depth=0;
        System.out.println("*************ROLLED BACK***************");
    }

    @Override
    public boolean isTransactionOpened() {
        return depth>0;
    }
}

















