package factory;

/**
 * Created by mshvarku on 15/04/2015.
 */
@Singleton
public class Tester2Impl implements Tester2 {
    @Inject
    private Tester tester;

    @Override
    @Transactional
    public void x() {
        System.out.println("Pink Floyd");
        tester.a();

    }
}
