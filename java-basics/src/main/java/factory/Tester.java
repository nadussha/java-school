package factory;

/**
 * Created by mshvarku on 15/04/2015.
 */
public interface Tester {
    void a();
    void b();
    void setTester(Tester tester);
}
