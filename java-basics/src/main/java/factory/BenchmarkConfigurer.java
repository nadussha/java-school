package factory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by mshvarku on 15/04/2015.
 */
public class BenchmarkConfigurer implements ProxyConfigurer {
    @Override
    public <T> T configure(final T t, final Class<T> type) {
        Method[] methods = type.getMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(Benchmark.class)) {
                return (T) Proxy.newProxyInstance(type.getClassLoader(), type.getInterfaces(), new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        Method originalMethod = type.getMethod(method.getName(), method.getParameterTypes());

                        if (originalMethod.isAnnotationPresent(Benchmark.class)) {
                            System.out.println("*************BENCHMARK*****************");
                            long before = System.nanoTime();
                            Object retVal = method.invoke(t, args);
                            long after = System.nanoTime();
                            System.out.println(method.getName() + " time: " + (after - before));
                            System.out.println("*************BENCHMARK*****************");
                            return retVal;
                        } else {
                            return method.invoke(t, args);
                        }
                    }
                });
            }
        }

        return t;

    }
}
