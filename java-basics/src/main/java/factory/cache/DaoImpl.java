package factory.cache;

/**
 * Created by mshvarku on 15/04/2015.
 */
public class DaoImpl implements Dao {

    private String[] names = {"Rodik", "Sergey", "Kolya", "Borya", "Alena", "Nadya", "Denis", "Pasha"};

    @Override
    public String getPersonName(int id) throws InterruptedException {
        Thread.sleep(2000);
        return names[id];
    }
}
