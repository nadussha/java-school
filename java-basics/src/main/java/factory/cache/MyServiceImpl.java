package factory.cache;

import factory.Benchmark;
import factory.Inject;
import factory.ObjectFactory;

/**
 * Created by mshvarku on 15/04/2015.
 */
@Benchmark
public class MyServiceImpl implements MyService {
    @Inject
    private Dao dao;

    @Override
    public void printPersonName(int id) throws InterruptedException {
        System.out.println(dao.getPersonName(id));
    }


    public static void main(String[] args) throws Exception {
        MyService myService = ObjectFactory.getInstance().createObject(MyService.class);
        myService.printPersonName(1);
        myService.printPersonName(2);
        myService.printPersonName(1);
    }
}
