package factory.cache;

/**
 * Created by mshvarku on 15/04/2015.
 */
public interface Dao {
    String getPersonName(int id) throws InterruptedException;
}
