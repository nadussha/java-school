package factory.cache;

/**
 * Created by mshvarku on 15/04/2015.
 */
public interface MyService {
    void printPersonName(int id) throws InterruptedException;
}
