package factory;

/**
 * Created by mshvarku on 15/04/2015.
 */
public interface ProxyConfigurer {
    <T> T configure(T t,Class<T> type);
}
