package factory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by mshvarku on 15/04/2015.
 */
public class TransactionConfigurer implements ProxyConfigurer {

    private TransactionManager transactionManager = TransactionManagerImpl.getInstance();


    @Override
    public <T> T configure(final T t, final Class<T> type) {
        Method[] methods = type.getMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(Transactional.class)) {
                return (T) Proxy.newProxyInstance(type.getClassLoader(), type.getInterfaces(), new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        Method originalMethod = type.getMethod(method.getName(), method.getParameterTypes());

                        if (originalMethod.isAnnotationPresent(Transactional.class)) {
                            boolean requireNew = originalMethod.getAnnotation(Transactional.class).requireNew();
                            if (requireNew || !transactionManager.isTransactionOpened()) {
                                transactionManager.openTransaction();
                                Object retVal = null;
                                try {
                                    retVal = method.invoke(t, args);
                                    transactionManager.commit();
                                    return retVal;
                                } catch (Exception e) {
                                    transactionManager.rollback();
                                    throw new RuntimeException(e);
                                }
                            }

                        }
                        return method.invoke(t, args);
                    }


                });
            }
        }

        return t;

    }
}
