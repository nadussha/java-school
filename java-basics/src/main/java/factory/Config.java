package factory;

/**
 * Created by mshvarku on 14/04/2015.
 */
public interface Config {
    <T> Class<? extends T> getImpl(Class<T> ifc);
}
