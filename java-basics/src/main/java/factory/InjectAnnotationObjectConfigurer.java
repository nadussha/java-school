package factory;

import java.lang.reflect.Field;
import java.util.Random;

/**
 * Created by mshvarku on 14/04/2015.
 */
public class InjectAnnotationObjectConfigurer implements ObjectConfigurer {
    @Override
    public void configure(Object t) throws Exception {
        Field[] fields = t.getClass().getDeclaredFields();
        for (Field field : fields) {
            Inject annotation = field.getAnnotation(Inject.class);
            if (annotation != null) {
                field.setAccessible(true);
                field.set(t,ObjectFactory.getInstance().createObject(field.getType()));
            }
        }
    }
}
