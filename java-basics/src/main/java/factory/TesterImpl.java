package factory;

import javax.annotation.PostConstruct;

/**
 * Created by mshvarku on 15/04/2015.
 */
@Singleton
public class TesterImpl implements Tester {
    @Self
    private Tester tester;

    public void setTester(Tester tester) {
        this.tester = tester;
    }

    public TesterImpl() {
        System.out.println("I'm constructor");
    }

    @PostConstruct
    public void init(){
        System.out.println("I'm init");
    }


    @Override
    @Transactional(requireNew = true)
    public void a() {
        System.out.println("AAAAAAAAAAA");
        tester.b();
    }

    @Override
    @Transactional(requireNew = true)
    public void b() {
        System.out.println("BBBBBBBBBBB");
    }
}
