package factory;

import org.reflections.Reflections;

import javax.annotation.PostConstruct;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by mshvarku on 14/04/2015.
 */
public class ObjectFactory {
    private static ObjectFactory ourInstance;

    static {
        ourInstance = new ObjectFactory();
        ourInstance.scanForObjectConfigurers();
        ourInstance.scanForProxyConfigurers();
        ourInstance.createSingletons();
    }

    public static ObjectFactory getInstance() {
        return ourInstance;
    }

    private Map<Class, Object> singletonMap = new HashMap<>();

    private Config config = new JavaConfig();
    private Reflections reflections = new Reflections("factory");
    private List<ObjectConfigurer> objectConfigurers = new ArrayList<>();
    private List<ProxyConfigurer> proxyConfigurers = new ArrayList<>();

    private ObjectFactory() {

    }

    public void createSingletons() {
        Set<Class<?>> singletonClasses = reflections.getTypesAnnotatedWith(Singleton.class);
        for (Class<?> singletonClass : singletonClasses) {
            try {
                singletonMap.put(singletonClass, createObject(singletonClass));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void scanForProxyConfigurers() {
        Set<Class<? extends ProxyConfigurer>> proxyConfigurerClasses = reflections.getSubTypesOf(ProxyConfigurer.class);
        for (Class<? extends ProxyConfigurer> proxyConfigurerClass : proxyConfigurerClasses) {
            try {
                proxyConfigurers.add(proxyConfigurerClass.newInstance());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void scanForObjectConfigurers() {
        Set<Class<? extends ObjectConfigurer>> classes = reflections.getSubTypesOf(ObjectConfigurer.class);
        for (Class<? extends ObjectConfigurer> configurerClass : classes) {
            try {
                objectConfigurers.add(configurerClass.newInstance());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }


    public <T> T createObject(Class<T> type) throws Exception {

        type = defineRealType(type);
        if (!singletonMap.containsKey(type)) {
            T t = type.newInstance();
            configure(t);
            invokeInitMethods(type, t);
            T originalObject = t;
            t = configureProxy(type, t);
            handleSelfInjection(type, t, originalObject);
            if (type.isAnnotationPresent(Singleton.class)) {
                singletonMap.put(type, t);
            }
            return t;
        }



        return (T) singletonMap.get(type);
    }

    private <T> Class<T> defineRealType(Class<T> type) {
        if (type.isInterface()) {
            type = findImpl(type);
        }
        return type;
    }

    private <T> void handleSelfInjection(Class<T> type, T t, T originalObject) throws IllegalAccessException {
        Field[] fields = type.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Self.class)) {
                field.setAccessible(true);
                field.set(originalObject,t);
            }
        }
    }

    private <T> T configureProxy(Class<T> type, T t) {
        for (ProxyConfigurer proxyConfigurer : proxyConfigurers) {
            t = proxyConfigurer.configure(t, type);
        }
        return t;
    }


    private <T> void invokeInitMethods(Class<T> type, T t) throws IllegalAccessException, InvocationTargetException {
        Method[] methods = type.getMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(PostConstruct.class)) {
                method.invoke(t);
            }
        }
    }


    private <T> void configure(T t) throws Exception {
        for (ObjectConfigurer configurer : objectConfigurers) {
            configurer.configure(t);
        }
    }


    private <T> Class<T> findImpl(Class<T> type) {
        Class<? extends T> impl = config.getImpl(type);
        if (impl == null) {
            Set<Class<? extends T>> classes = reflections.getSubTypesOf(type);
            if (classes.size() != 1) {
                throw new RuntimeException("0 or more than one impl found to interface " + type + " define the wanted impl in config");
            }
            type = (Class<T>) classes.iterator().next();
        } else {
            type = (Class<T>) impl;
        }
        return type;
    }


}
