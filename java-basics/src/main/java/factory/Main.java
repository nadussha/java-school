package factory;

/**
 * Created by mshvarku on 13/04/2015.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Tester tester = ObjectFactory.getInstance().createObject(Tester.class);
        tester.a();
    }
}
